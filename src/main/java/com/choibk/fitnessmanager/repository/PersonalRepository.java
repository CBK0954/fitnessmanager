package com.choibk.fitnessmanager.repository;

import com.choibk.fitnessmanager.entity.PersonalCustomer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonalRepository extends JpaRepository<PersonalCustomer, Long> {
}
