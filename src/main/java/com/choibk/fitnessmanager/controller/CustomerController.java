package com.choibk.fitnessmanager.controller;

import com.choibk.fitnessmanager.model.CustomerInfoUpdateRequest;
import com.choibk.fitnessmanager.model.CustomerItem;
import com.choibk.fitnessmanager.model.CustomerRequest;
import com.choibk.fitnessmanager.model.CustomerWeightUpdateRequest;
import com.choibk.fitnessmanager.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/customer")
public class CustomerController {
    private final CustomerService customerService;

    @PostMapping("/data")
    public String setCustomer(@RequestBody @Valid CustomerRequest request) {
        customerService.setPersonalCustomer(request);
        return "Done";
    }
    @GetMapping("/all")
    public List<CustomerItem> getCustomers() {
        List<CustomerItem> result = customerService.getCustomers();

        return result;
    }

    @PutMapping("/info/id/{id}")
    public String putCustomerInfo(@PathVariable long id, @RequestBody @Valid CustomerInfoUpdateRequest request) {
        customerService.putCustomerInfo(id, request);

        return "Done";
    }

    @PutMapping("/weight/id/{id}")
    public String putCustomerWeight(@PathVariable long id, @RequestBody @Valid CustomerWeightUpdateRequest request) {
        customerService.putCustomerWeight(id, request);

        return "Done";
    }

    @PutMapping("/visit/id/{id}")
    public String putCustomerVisit(@PathVariable long id) {
        customerService.putCustomerVisit(id);

        return "Done";
    }
}
