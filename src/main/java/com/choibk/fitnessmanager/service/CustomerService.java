package com.choibk.fitnessmanager.service;

import com.choibk.fitnessmanager.entity.PersonalCustomer;
import com.choibk.fitnessmanager.model.CustomerInfoUpdateRequest;
import com.choibk.fitnessmanager.model.CustomerItem;
import com.choibk.fitnessmanager.model.CustomerRequest;
import com.choibk.fitnessmanager.model.CustomerWeightUpdateRequest;
import com.choibk.fitnessmanager.repository.PersonalRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CustomerService {
    private final PersonalRepository personalRepository;

    public void setPersonalCustomer(CustomerRequest request) {
        PersonalCustomer addData = new PersonalCustomer();
        addData.setCustomerName(request.getCustomerName());
        addData.setCustomerPhone(request.getCustomerPhone());
        addData.setHeight(request.getHeight());
        addData.setWeight(request.getWeight());
        addData.setDateFirst(LocalDateTime.now());

        personalRepository.save(addData);
    }

    public List<CustomerItem> getCustomers() {
        List<PersonalCustomer> originList = personalRepository.findAll();

        List<CustomerItem> result = new LinkedList<>();

        for(PersonalCustomer item : originList) {
            CustomerItem addItem = new CustomerItem();
            addItem.setId(item.getId());
            addItem.setCustomerName(item.getCustomerName());
            addItem.setCustomerPhone(item.getCustomerPhone());
            addItem.setHeight(item.getHeight());
            addItem.setWeight(item.getWeight());
            addItem.setDateFirst(item.getDateFirst());
            addItem.setDataLast(item.getDateLast());

            float heightDouble = (item.getHeight() / 100) * (item.getHeight() / 100);
            double bmi = item.getWeight() / heightDouble;

            addItem.setBmi(bmi);

            String bmiResultText = "";
            if(bmi >= 18.5) {
                bmiResultText = "저체중";
            } else if (bmi <= 22.9) {
                bmiResultText = "정상";
            } else if (bmi <= 24.9 ) {
                bmiResultText = "과체중";
            } else {
                bmiResultText = "비만";
            }

            addItem.setBmiResultName(bmiResultText);

            result.add(addItem);
        }
            return result;
    }

    public void putCustomerInfo(long id, CustomerInfoUpdateRequest request) {
        PersonalCustomer originData = personalRepository.findById(id).orElseThrow();
        originData.setCustomerName(request.getCustomerName());
        originData.setCustomerPhone(request.getCustomerPhone());

        personalRepository.save(originData);
    }

    public void putCustomerWeight(long id, CustomerWeightUpdateRequest request) {
        PersonalCustomer originData = personalRepository.findById(id).orElseThrow();
        originData.setWeight(request.getHeight());

        personalRepository.save(originData);
    }

    public void putCustomerVisit(long id) {
        PersonalCustomer originData = personalRepository.findById(id).orElseThrow();
        originData.setDateLast(LocalDateTime.now());

        personalRepository.save(originData);
    }
}